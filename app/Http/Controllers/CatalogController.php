<?php

namespace App\Http\Controllers;

use App\Models\Catalog;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatalogController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $catalog = Catalog::with('products')->get();
        return response()->json([
            'response' => $catalog
        ]);
    }


    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'category' => 'required',
            'products_id' => 'required'
        ]);

        if ($validator->fails()) {
            // If validation fails, return the error response
            return response()->json(['errors' => $validator->errors()], 400);
        }

        // The request data has passed validation, continue with saving data

        // Check if the product with the provided ID exists
        $product = Product::find($request->input('products_id'));
        if (!$product) {
            // If product does not exist, return error response
            return response()->json(['message' => 'Product not found for the given ID'], 404);
        }

        // The product exists, continue with creating catalog
        $catalog = Catalog::create([
            'category' => $request->input('category'),
            'products_id' => $request->input('products_id'),
        ]);

        return response()->json([
            'response' => $catalog,
            'message' => 'Catalog added successfully',
        ]);
    }


    public function show(Catalog $catalog)
    {
        return response()->json([
            'response' => $catalog,
        ]);
    }



    public function update(Request $request, Catalog $catalog)
    {
        $catalog->category = $request->category;
        $catalog->products_id = $request->products_id;
        $catalog->save();

        return response()->json([
            'response' => $catalog
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Catalog $catalog)
    {
        $catalog->delete();
        return response()->json([
            'message' => 'Catalog deleted'
        ]);
    }
}
