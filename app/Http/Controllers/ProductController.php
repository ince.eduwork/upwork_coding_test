<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $product = Product::all();
        return response()->json([
            'response' => $product
        ]);
    }


    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'img_url' => 'required',
            'description' => 'required',
            'product_price' => 'required|numeric',
            'product_cost' => 'required|numeric',
            'unit' => 'required',
            'weight_per_unit' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            // If validation fails, return the error response
            return response()->json(['errors' => $validator->errors()], 400);
        }

        // The request data has passed validation, continue with saving data
        $product = Product::create([
            'name' => $request->input('name'),
            'img_url' => $request->input('img_url'),
            'description' => $request->input('description'),
            'product_price' => $request->input('product_price'),
            'product_cost' => $request->input('product_cost'),
            'unit' => $request->input('unit'),
            'weight_per_unit' => $request->input('weight_per_unit'),
        ]);

        return response()->json([
            'response' => $product,
            'message' => 'product added successfully',
        ]);
    }


    public function show(Product $product)
    {
        return response()->json([
            'response' => $product,
        ]);
    }



    public function update(Request $request, Product $product)
    {
        $product->name = $request->name;
        $product->img_url = $request->img_url;
        $product->description = $request->description;
        $product->product_price = $request->product_price;
        $product->product_cost = $request->product_cost;
        $product->unit = $request->unit;
        $product->weight_per_unit = $request->weight_per_unit;
        $product->save();

        return response()->json([
            'response' => $product
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return response()->json([
            'message' => 'Product deleted'
        ]);
    }
}
